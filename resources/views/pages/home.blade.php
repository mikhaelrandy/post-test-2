 @extends('layouts.frontend.master')
 @section('content')
 <!-- Introduction Section -->
 <section id="introduction" class="content-section clearfix">
	    <div class="flexslider introduction-slider">
		
		<ul class="slides">
		    <li> 
			<div class="flex-caption section-overlay slide-caption">
			    <div class="container">
				<div class="row">
				    <div class="col-md-12">
					<h2>
					    Hallo, Selamat datang<br>
						</h2>
					    <h1>PT Milenia Armada Ekspress</h1>
					<a class="slide-button">Read More</a>
				    </div>
				</div>
			    </div>
			</div> 
			<img src="images/slides/slide4.jpg" alt="slide-01">  
		    </li>
		    
		    <li> 
			<div class="flex-caption section-overlay slide-caption">
			    <div class="container">
				<div class="row">
				    <div class="col-md-12">
						<br>
						<br>
						<br>
						<br>
					<a class="slide-button">Details</a>
				    </div>
				</div>
			    </div>
			</div> 
			<img src="images/slides/slide8.jpg" alt="slide-02">    
		    </li>
		    
		    <li> 
			<div class="flex-caption section-overlay slide-caption">
			    <div class="container">
				<div class="row">
				    <div class="col-md-12">
						<br>
						<br>
						<br>
						<br>
					<a class="slide-button">More Info</a>
				    </div>
				</div>
			    </div>
			</div> 
			<img src="images/slides/slide7.jpg" alt="slide-03">    
		    </li>
		</ul>

		<div class="slide-navigation">
		    <a href="#" class="slide-prev"><i class="fa fa-arrow-left"></i></a>
		    <a href="#" class="slide-next"><i class="fa fa-arrow-right"></i></a>
		</div>
		
	    </div> <!-- //.flexslider -->
	</section>
	<!-- End Introduction Section -->
	
	<!-- About Section -->
	<section id="about" class="content-section clearfix">
	    <div class="container">
		<div class="row">
		    <div class="col-md-12">
			<h2 class="section-title left">Pelayanan Kami</h2>
		    </div>
		</div>
		<div class="row">
		    
		    <div class="col-md-4 animated-item feature-item fadeInUp" data-delay="200">
			<span class="feature-icon"><i class="li_bulb"></i></span>
			<div class="feature-info">
			    <h3 class="feature-title"><span></span>Innovation</h3>
			    <p>Fusce dolor ante, imperdiet vitae sapien eget, ullamcorper laoreet turpis. Integer posuere dui sapien. Morbi eu aliquam lectus.</p>
			</div>
		    </div>
		    
		    <div class="col-md-4 animated-item feature-item fadeInUp" data-delay="300">
			<span class="feature-icon"><i class="li_params"></i></span>
			<div class="feature-info">
			    <h3 class="feature-title"><span></span>Management</h3>
			    <p>Mauris quis egestas ipsum. Suspendisse auctor porttitor eros ut molestie. Proin quis nunc nec turpis ultrices aliquet eget ac neque.</p>
			</div>
		    </div>
		    
		    <div class="col-md-4 animated-item feature-item fadeInUp" data-delay="400">
			<span class="feature-icon"><i class="li_heart"></i></span>
			<div class="feature-info">
			    <h3 class="feature-title"><span></span>Quick Help</h3>
			    <p>Integer posuere dui sapien. Morbi eu aliquam lectus. Nam sem arcu, varius nec bibendum egestas, finibus tempus urna.</p>
			</div>
		    </div>
		    
		</div>
		
		
	    </div> <!-- //.container -->
	</section>
	<section id="counter" class="content-section content-overlay parallax-section clearfix">
		<div class="section-overlay">
		    <div class="container">
			<div class="row fadeInUp">
			    <div class="col-md-3 counter-item">
				<span class="counter-icon"><i class="li_user"></i></span>
				
				<span class="counter-text">Satisfied Clients</span>
			    </div>
			    
			    <div class="col-md-3 counter-item">
				<span class="counter-icon"><i class="li_display"></i></span>
				
				<span class="counter-text">Projects Delivered</span>
			    </div>
			    
			    <div class="col-md-3 counter-item">
				<span class="counter-icon"><i class="li_pen"></i></span>
				
				<span class="counter-text">Line of Code</span>
			    </div>
			    
			    <div class="col-md-3 counter-item">
				<span class="counter-icon"><i class="li_bubble"></i></span>
				
				<span class="counter-text">Questions Answered</span>
			    </div>
			</div>
		    </div>
		    
		</div>
	</section>
	<!-- End About Section -->
	
<!-- choose section -->
<div class="choose">
    <div class="container">
       <div class="row">
          <div class="col-md-12">
             <div class="titlepage">
                <h2 class="section-title left"> Mengapa Memilih Kami? </h2>
             </div>
          </div>
       </div>
       <div class="row">
          <div class="col-md-5">
             <div class="choose_box">
                <i><img src="images/why1.png" alt="#"/></i>
                <h3>VISI</h3>
                <p>Menjadi penyedia solusi logistik terpadu yang terpercaya, terluas, dan terkemuka di Indonesia</p>
             </div>
          </div>
          <div class="col-md-5 offset-md-2">
             <div class="choose_box">
                <i><img src="images/why2.png" alt="#"/></i>
                <h3>MISI</h3>
                <p>Membangun kemitraan usaha dengan mitra kerja strategis yang saling menguntungkan</p>
             </div>
          </div>
       </div>
    </div>
 </div>
 <!-- end choose section -->


	
	<!-- Partners Section -->
	<section id="partners" class="content-section content-overlay parallax-section clearfix">
	    
	    <div class="section-overlay">
		<div class="container">
		    <div class="row">
			<div class="col-md-12">
			    <h2 class="section-title left">Best Partners</h2>
			</div>
		    </div>
		    <div class="row no-margin">
			<div class="col-md-2 partner-logo">
			    <img data-toggle="tooltip" title="Partner 1" src="images/abc.png" alt="">
			</div>
			<div class="col-md-2 partner-logo">
			    <img data-toggle="tooltip" title="Our Partner 2" src="images/abc.png" alt="">
			</div>
			<div class="col-md-2 partner-logo">
			    <img data-toggle="tooltip" title="Partner Three" src="images/abc.png" alt="">
			</div>
			<div class="col-md-2 partner-logo">
			    <img data-toggle="tooltip" title="Partner 4" src="images/abc.png" alt="">
			</div>
			<div class="col-md-2 partner-logo">
			    <img data-toggle="tooltip" title="Our Partner 5" src="images/abc.png" alt="">
			</div>
			<div class="col-md-2 partner-logo">
			    <img data-toggle="tooltip" title="Partner 6" src="images/abc.png" alt="">
			</div>
		    </div>
		</div>
	    </div>
	    
	</section>
	<!-- End Partners Section -->
	
	<!-- Partners Section -->
	<section id="our-team" class="content-section clearfix">
	    <div class="container">
		<div class="row">
		    <div class="col-md-12">
			<h2 class="section-title right">Our Team</h2>
		    </div>
		</div>
		<div class="row">
		    <div class="col-md-3 team-box animated-item fadeInUp" data-delay="100">
			<div class="team-thumb">
			    <img src="images/author1.jpg" alt="">
			    <div class="team-overlay">
				<div class="team-desc">
				    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, nisi dui, ultrices.</p>
				</div>
				<ul class="team-social-icons">
				    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
				    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
				    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
				</ul>
			    </div>
			</div>
			<div class="team-info">
			    <h4>Tracy</h4>
			    <h6>Admin Market Online</h6>
			</div>
		    </div> <!-- //.team-box -->
		    
		    <div class="col-md-3 team-box animated-item fadeInUp" data-delay="200">
			<div class="team-thumb">
			    <img src="images/author2.jpg" alt="">
			    <div class="team-overlay">
				<div class="team-desc">
				    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, nisi dui, ultrices.</p>
				</div>
				<ul class="team-social-icons">
				    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
				    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
				    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
				</ul>
			    </div>
			</div>
			<div class="team-info">
			    <h4>Julia</h4>
			    <h6>Sales Marketing</h6>
			</div>
		    </div> <!-- //.team-box -->
		    
		    <div class="col-md-3 team-box animated-item fadeInUp" data-delay="300">
			<div class="team-thumb">
			    <img src="images/author3.jpg" alt="">
			    <div class="team-overlay">
				<div class="team-desc">
				    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, nisi dui, ultrices.</p>
				</div>
				<ul class="team-social-icons">
				    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
				    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
				    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
				</ul>
			    </div>
			</div>
			<div class="team-info">
			    <h4>Linda</h4>
			    <h6>Customer Service</h6>
			</div>
		    </div> <!-- //.team-box -->
		    
		    <div class="col-md-3 team-box animated-item fadeInUp" data-delay="400">
			<div class="team-thumb">
			    <img src="images/author4.jpg" alt="">
			    <div class="team-overlay">
				<div class="team-desc">
				    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, nisi dui, ultrices.</p>
				</div>
				<ul class="team-social-icons">
				    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
				    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
				    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
				</ul>
			    </div>
			</div>
			<div class="team-info">
			    <h4>Christina</h4>
			    <h6>CEO</h6>
			</div>
		    </div> <!-- //.team-box -->
		</div>
	    </div>
	</section>
	<!-- End Partners Section -->
	
	
	
	<!-- Contact Section -->
	<section id="contact" class="content-section">
	    <div class="container">
		<div class="row">
		    <div class="col-md-12">
			<h2 class="section-title left">Contact Us</h2>
		    </div>
		</div>
		<div class="row">
		    <div class="col-md-6 contact-form">
			<form id="email-form" action="#" method="post">
			    <div class="row">
				<div class="col-md-6">
				    <label for="name">Name</label>
				    <input type="text" class="form-input" name="name" id="name">
				</div>
				<div class="col-md-6">
				    <label for="email">Email</label>
				    <input type="email" class="form-input" name="email" id="email">
				</div>
			    </div>
			    <div class="row">
				<div class="col-md-12">
				    <label for="message">Message</label>
				    <textarea rows="6" id="message" name="message" class="form-input"></textarea>
				</div>
			    </div>
			    <div class="row">
				<div class="col-md-12">
				    <button class="main-button" type="submit">Send Message</button>
				</div>
			    </div>
			</form>
			
		    </div>
		    <div class="col-md-5 col-md-offset-1 contact-info">
			<h4>KANTOR PUSAT</h4>
			<p>Jl. Pakin Raya, Rukan Mitra Bahari
				Blok A/15, Kel. Penjaringan Jakarta
				Utara 14440 - Indonesia
			</p>
			
			<h4>Contact Details</h4>
			<ul>
			    <li><i class="fa fa-map-marker"></i>Jakarta Utara</li>
			    <li><i class="fa fa-phone"></i>010-020-0340</li>
			    <li><i class="fa fa-envelope"></i><a href="mailto:info@company.com">info@company.com</a></li>
			</ul>
			
		    </div>
		</div>
	    </div>
	</section>
	<!-- End Contact Section -->
	
	<!-- Hidden Content -->
	<section id="load-folio" class="folio-details">
	    <div id="folio-content" class="load-folio-content">
		
	    </div>
	</section>
 @stop 