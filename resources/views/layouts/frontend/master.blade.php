<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> 
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> 
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
    	
        <title>Milenia Armada Ekspress</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta charset="UTF-8">

        <!-- CSS Bootstrap & Custom -->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
		<link href="css/animate.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
       
        <!-- JavaScripts -->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/modernizr.js"></script>
        <!--[if lt IE 8]>
	    <div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
        </div>
        <![endif]-->
    </head>
    <body>
	<!-- Preloader -->
	<div id="page-preloader">
	    <div id="spinner"></div>
	</div>
	<!-- End Preloader -->
	
	<!-- Mobile Menu -->
	<div id="mobile-menu" class="mobile-nav hide-nav hidden-md hidden-lg">
	    <div class="mobile-menu-close">
		<button type="button" class="close" aria-hidden="true"><span>Close</span>&times;</button>
	    </div>
	   
		<ul class="mobile-navigation">
			<li><a href="#introduction">BERANDA</a></li>
			<li><a href="#tentang">TENTANG KAMI</a></li>
			<li><a href="#produk">PRODUK & LAYANAN KAMI</a></li>
			<li><a href="#hubungi">HUBUNGI KAMI</a></li>
		</ul>
		
		<ul class="mobile-social-icons">
		    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
		    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
		    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
		    <li><a href="#"><i class="fa fa-rss"></i></a></li>
		</ul>
	    
	</div>

        <!-- Header -->
        <header id="header" class="site-header" role="banner">
	    <div class="container">
		<div class="row">
		    
		    <div class="col-md-4 logo">
			<a href="#">
			    <img src="images/MAX-LOGO.gif" alt="Impressum Template" title="Impressum Parallax Template">
			</a>
		    </div> <!-- //.logo -->
		    
		    <div class="col-md-8">
			<nav id="navigation" class="hidden-sm hidden-xs">
			    <ul id="main-nav" class="main-navigation">
				<li class="current"><a href="#introduction">BERANDA</a></li>
				<li><a href="#tentang">TENTANG KAMI</a></li>
				<li><a href="#produk">PRODUK & LAYANAN KAMI</a></li>
				<li><a href="#hubungi">HUBUNGI KAMI</a></li>
			    </ul>
			</nav>
			
			<a href="#mobile-menu" class="visible-xs visible-sm mobile-menu-trigger"><i class="fa  fa-reorder"></i></a>
			
		    </div>
		    
		</div> <!-- //.row -->
	    </div> <!-- //.container -->
            
        </header>
	<!-- End Header -->
	
	@yield('content')
	
        <footer id="footer" class="site-footer" role="contentinfo">
			<div class="container">
                <div class="col-md-6 col-sm-6">
                    <div class="copyright-text">
                        <span>Copyright &copy; 2022 PT Milenia Armada Ekspress</span>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="social-icons">
                        <ul>
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-linkedin"></a></li>
                            <li><a href="#" class="fa fa-rss"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
	
        <!-- JavaScripts -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/custom.js"></script>

    </body>
</html>